# imex
imex (image eXtractor), a google chrome extension that lets users extract images from web pages.

# download and install
~~extension can be downloaded and installed at https://chrome.google.com/webstore/detail/imex/dhahfmndmmgilfdlfilnmmjpobgkpjmp~~.

## skeleton version
want to make your own extension? this might be an ok start:
[view ripped skeleton version](https://gitlab.com/pqq/imex/tree/529b6cbaa5142686476cb7cd79b4664601f0a847/imex)

## nifty resources
https://developer.chrome.com/extensions/api_index

https://developer.chrome.com/extensions/tabs

https://developer.chrome.com/extensions/samples

https://github.com/klevstul/klevstul.github.com/wiki/codeStandard
