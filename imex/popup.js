chrome.extension.getBackgroundPage().debug("popup.js");

function doImex(event) {
    chrome.extension.getBackgroundPage().debug("doImex()");

    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        chrome.extension.getBackgroundPage().debug("chrome.tabs.query(tab.id=" + tabs[0].id + ", tab.url='"  + tabs[0].url + "')");

        // make sure the tab we are going to do an imex on has finished loading
        if (tabs[0].status === "complete"){

            // send a message to the tab where we have injected a content script (content.js), which is waiting for our message
            chrome.tabs.sendMessage(tabs[0].id, {action: "getImages"}, function(contentResponse){
            //chrome.extension.getBackgroundPage().debug("response: " + contentResponse.dom);

                // create a new tab, that will be our result page
                chrome.tabs.create({url : "result.html"}, function(tab){
                    chrome.extension.getBackgroundPage().debug("chrome.tabs.create(tab.id="+tab.id+", tab.url='result.html')");

                    // while our result page is loading...
                    if (tab.status === "loading"){
                        // we're sending a message. the message is the stuff we gathered from the tab / page that "imex'ed"
                        chrome.tabs.sendMessage(
                              tab.id
                            , {"action" : "extraxtImages", "passedData" : contentResponse.retValue}
                            , function(resultResponse){
                                // by some reason, we never get to this function. the good thing is, it doesn't matter. still annoys me not knowing why though.
                                // two theories:
                                // - nested sendMessage() calls is creating som confusion
                                // - content.js gets injected into result.html as well, and this causes some confusion
                                // (note: if you open extension's popup.html in own tab, and runs imex, then this is NOT lost in cyperspace.)
                                chrome.extension.getBackgroundPage().debug("lost in cyberspace!");
                            }
                        );
                    }

                });

            });

        }

    });

}

// wait for the click
// "And I didn't have any idea what to, do but I knew I needed a click, so we put a click on the 24 track which then was synced to the moog modular."
document.addEventListener('DOMContentLoaded', function () {
    var imex = document.getElementById("imex");
    if (imex) {
        imex.addEventListener('click', doImex);
    }
});
