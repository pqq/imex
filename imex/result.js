chrome.extension.getBackgroundPage().debug("result.js");

function createImageElement(src) {
    var img = document.createElement("img");
    img.src     = src;
    img.alt     = src;
    img.title   = src;

    document.body.appendChild(img);
}

function createLinkElement(src) {
    var a           = document.createElement("a");
    var linkText    = document.createTextNode(src);
    a.appendChild(linkText);
    a.href          = src;
    a.target        = "_blank";

    document.body.appendChild(a);
}

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    chrome.extension.getBackgroundPage().debug("result.js - message listener");

    var result      = document.getElementById("resultOutput");
    var passedData  = request.passedData;

    if (passedData != null){
        for (var i  = 0; i < passedData.length; ++i) {
            createImageElement(passedData[i]);
            createLinkElement(passedData[i]);
            document.body.appendChild(document.createElement("br"));
            document.body.appendChild(document.createElement("br"));
         }
     }

    sendResponse("i am always lost in cyberspace, poor me...");
});
