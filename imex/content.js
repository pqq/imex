/*
    documentation of the dom document
        https://developer.mozilla.org/en-US/docs/Web/API/Document

    documentation of regexp
        https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp

    svg image element
        doc:
            https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/SVG_Image_Tag
        example:
            <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://lh4.googleusercontent.com/5pACCoTtcsAuVGCOlwL799KgAFI0Qym5iYqVIsQsBnGpbm7X3zNKxrehavy7gqyrIUZ9CD5IoMUeDtafxlCK82SjhOhnwn4A5fXmTUpa_ZZvzhPidgUNgA4Bgzlyj0G2YcuO4ho" width="100%" height="100%" preserveAspectRatio="none"><title></title><desc></desc></image>
        dom:
            image.href.baseval

    notes
        remember: it is possible to break, by adding the command "debugger;" somwhere in the code

        access background page's console log:
            chrome.extension.getBackgroundPage().console.log("hello, world!");

        i clearly do lack some basic dom and js knowledge, because i did struggle a bit getting this code to work. not sure if the below observations are correct either.

        some observations while working with google documents:
            first off, when doing a "document.body.innerHTML", it seems like we got "unrendered source code", from a loaded but not rendered page.
            while "document.body" other the other hand gave rendered code.

            hence, the code
                var svgImageElements = document.body.querySelectorAll("image");
                or
                var allImages = document.images;
            did not work, as i thought it would. if i did load the page, and did an inspect element (to get the page rendered?!), and re-ran imex, then it did work. otherwise not.
*/

//console.log("DEBUG: content.js");

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {

    var images                  = [];
    var imagesReturn            = [];
    var seenImages              = {};
    var htmlCodePreRendering    = document.body.innerHTML;
    var googleSvgImageLink      = /https?:\/\/\w*\.googleusercontent\.\w+(\/(\w+|-*)*)*/gim;

    images = htmlCodePreRendering.match(googleSvgImageLink);

    // loop through all images, and save them (only once)
    if (images != null){
        for (var i  = 0; i < images.length; ++i) {
            if (seenImages[images[i]]){
                continue;
            }
            seenImages[images[i]] = 1;
            imagesReturn.push(images[i]);
         }
     }

    if (request.action && (request.action == "getDom")) {
        sendResponse({ retValue: document.body.innerHTML });
    } else if (request.action && (request.action == "getImages")) {
            sendResponse({ retValue: imagesReturn });
    } else {
        sendResponse("content.js speaking. i'm truly sorry, but did not understand your request.");
    }
});
